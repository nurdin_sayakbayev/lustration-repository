**Преамбула**
*Мы признаем, что страна корумпирована, связана с криминалом и мафией. И хотим избавится от пороков, оставив их в прошлом. Поэтому, мы будем проводить реформы с новым мышлением и в течение 6 лет не будем допускать к выработке и реализации реформ носителей прежнего сознания, прежнх ценностей и норм поведения.* 
Статья 1. 
С целью ограничения влияния на процесс реформ для носителей коррупционного сознания, лицам, указанным в статье "4.Кто не допускается" настоящего закона запрещается занимать должности, указанные в статье 5."Должности, на которые не допускаются".

Статья 4.
Кто не допускается.
Судимые за преступления.
Работники таможни и правоохранительныз органов, которые проработали более года в период с 1991 по 2020 год. Работники судов. 
Депутаты.
Министры. Президенты и пр.

Статья 5.
Должности на которые не допускаются носители прежнего сознания.
Президент. Депутаты парламента. Судьи. Работники прокуратуры. Руководители правоохранительных органов и руководители их территориальных подразделений, а также их заместители. Министры и их замы. Премьер-министр. Губернаторы областей. районов. Представители правительства. Руководитель аппарата президента. Члены ЦИК. Руководители госпредприятий и муниципальных предприятий. 


Лица, запятнавшие свою репутацию причастностью к коррупции. Запятнана ли репутация определяется Комиссией по люстрации. 

На время действия настоящего закона создается независимая комисмия по люстрации. Состав комиссии утверждается ежегодно. В комисмию не могут входить лица, коьорые подпадают под ограничения, а также бывшие или действующие государственные служашие, действующие военослужащие и пенсионеры минобороны  

Состав комиссии обсуждается еа общественных слушаниях и каждая кандидатура утверждается парламентом. Комисмия имеет право получения всей информации, включая носударсьвенеуё и коммерчесеуб тайну. Имеет досьуп ко всем архивам и может делать официальные запросы, обязатнльные для исполнения как государствннныии, так и частными лицами. 
Заключнния комиссии публикуются, могут содержать секреьную часть, которая не публикуется. 
Все фракции могут направить своего человека в Комиссию 
Решения комиссии принимаются концессусом. Если концессуса нет, то проводится повторное голосование и решение принимается большинством.

Количество членов комиссии 9. 
Наблюдатели от общественности.

Решения комиссии могут быть обжалованы в конституционный суд.

Исполнение закона о люстрации может быть обжаловано в Комисмию 
Можно делать запросы в комиссию по конкретным кейсам и комиссия дает разъясненмя, которые публикуются. 
Председатель комисии должен иметь высшее юридическое образование.
